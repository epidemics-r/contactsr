% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/timeuse.R
\name{activity_by_location_matrix}
\alias{activity_by_location_matrix}
\title{Calculate location and activity matrices based on activity weight data}
\usage{
activity_by_location_matrix(data, age_limits, demography)
}
\arguments{
\item{data}{The time use data split according to polymod location, activity and activity weight \link{timeuse}.}

\item{age_limits}{The age limits to use. Age limits are given as a vector giving the intermediate ages, e.g. 'c(15,45,65)' would split the data into age groups [0,15), [15,45), [45,65), [65,+).}

\item{demography}{Demographic data needed to properly weight the age groups by population size \link{demography_uk_2020}.}
}
\value{
A list of locations, with each location containing a list of matrices by activity. The matrices contain the weight for each activity (in each location).
}
\description{
Calculate location and activity matrices based on activity weight data
}
\examples{
activity_by_location_matrix(contactsr::timeuse \%>\% dplyr::filter(Country == "United Kingdom"),
                            c(5,15,65), contactsr::demography_uk_2020)

}
