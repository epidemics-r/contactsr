
# contactsr

<!-- badges: start -->
<!-- badges: end -->

The goal of contactsr is to allow easy access to the Polymod and BBC data set and apply different intervention strategies

## Installation

You can install the latest version of contactsr by cloning this repository and installing it using:

``` r
devtools::install(path/to/cloned/repository)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(contactsr)
data(demography_uk_2020)
load_contact_data(source = "polymod", type = "physical", age_limits = c(1,5,25,45,65), exclude_location = "school", demography = demography_uk_2020)
```

